// import 'package:flutter/material.dart';
//
// class HomeScreen extends StatefulWidget {
//   const HomeScreen({Key? key}) : super(key: key);
//
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }
//
// class _HomeScreenState extends State<HomeScreen> {
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Text('Home'),
//     );
//   }
//
//
// }
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/providers/pet_provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              Tab(text: '首頁'),
              Tab(text: '最愛'),
              Tab(text: '尋找遺失'),
            ],
          ),
          title: const Text('首頁'),
        ),
        body: TabBarView(
          children: [
            FirstPage(),
            Icon(Icons.directions_transit),
            Icon(Icons.directions_bike),
          ],
        ),
      ),
    );
  }
}

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  @override
  void initState() {
    super.initState();
  }

  // init() async {
  //   try {
  //     List<Pet> temp = await context.read(petRepositoryProvider).fetchPets();
  //
  //     setState(() {
  //       pets.addAll(temp);
  //     });
  //   } catch (e) {
  //     ScaffoldMessenger.of(context)
  //         .showSnackBar(SnackBar(content: Text('fetch error')));
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        AsyncValue<List<Pet>?> petsAsyncValue = watch(petListFutureProvider);
        return petsAsyncValue.when(
            data: (pets) {
              return ListView.builder(
                itemCount: pets!.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                    child: Card(
                      clipBehavior: Clip.antiAlias,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 120,
                            height: 120,
                            child: CachedNetworkImage(
                              imageUrl: pets[index].albumFile!,
                              width: 120,
                              height: 120,
                              fit: BoxFit.cover,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                            // child: Image.network(pets[index].albumFile!),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.arrow_drop_down_circle),
                                  title: const Text('Card title 1'),
                                  subtitle: Text(
                                    'Secondary Text',
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(0.6)),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Text(
                                    'Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(0.6)),
                                  ),
                                ),
                                ButtonBar(
                                  alignment: MainAxisAlignment.start,
                                  children: [
                                    FlatButton(
                                      textColor: const Color(0xFF6200EE),
                                      onPressed: () {
                                        // Perform some action
                                      },
                                      child: const Text('ACTION 1'),
                                    ),
                                    FlatButton(
                                      textColor: const Color(0xFF6200EE),
                                      onPressed: () {
                                        // Perform some action
                                      },
                                      child: const Text('ACTION 2'),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                  return ListTile(
                    title: Text(pets[index].animalId.toString()),
                  );
                },
              );
            },
            loading: () => CircularProgressIndicator(),
            error: (e, s) => Text(e.toString()));
      },
    );
  }
// return FutureBuilder<List<Pet>>(
//   future: context
//       .read(petRepositoryProvider)
//       .fetchPets(), // a previously-obtained Future<String> or null
//   builder: (BuildContext context, AsyncSnapshot<List<Pet>> snapshot) {
//     if (snapshot.hasData) {
//       return ListView.builder(
//         itemCount: snapshot.data!.length,
//         itemBuilder: (context, index) {
//           return ListTile(
//             title: Text(snapshot.data![index].animalId.toString()),
//           );
//         },
//       );
//     } else if (snapshot.hasError) {
//       return Icon(
//         Icons.error_outline,
//         color: Colors.red,
//         size: 60,
//       );
//     } else {
//       //loading
//       return Column(
//         children: [
//           SizedBox(
//             child: CircularProgressIndicator(),
//             width: 60,
//             height: 60,
//           ),
//           Padding(
//             padding: EdgeInsets.only(top: 16),
//             child: Text('Awaiting result...'),
//           )
//         ],
//       );
//     }
//   },
// );
  // }
}
