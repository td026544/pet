import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/service/auth_provider.dart';
import 'package:pets/view/register_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('登入'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 180,
              child: TextFormField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: "Email",
                  // prefixIcon: Icon(Icons.person),
                  hintText: '輸入email',
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: 180,
              child: TextFormField(
                controller: _pwdController,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: "Password",
                  // prefixIcon: Icon(Icons.admin_panel_settings_sharp),
                  hintText: '輸入密碼',
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            ElevatedButton(
                onPressed: () {
                  String email = emailController.text;
                  String password = _pwdController.text;
                  context
                      .read(authServiceProvider)
                      .signInWithEmailAndPassword(email, password);
                },
                child: Text('登入')),
            OutlinedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => RegisterScreen(),
                  ),
                );
              },
              child: Text('註冊'),
            ),
          ],
        ),
      ),
    );
  }
}
