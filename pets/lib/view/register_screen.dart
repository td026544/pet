import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/service/auth_provider.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    emailController.dispose();
    _pwdController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('註冊'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 180,
                child: TextFormField(
                    controller: emailController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: "Email",
                      prefixIcon: Icon(Icons.person),
                      hintText: '輸入email',
                    ),
                    validator: (email) {
                      if (email != null) {
                        return email.trim().length > 0 ? null : "email不能为空";
                      }
                    }),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: 180,
                child: TextFormField(
                    controller: _pwdController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Password",
                      prefixIcon: Icon(Icons.admin_panel_settings_sharp),
                      // border: OutlineInputBorder(),
                      hintText: '輸入密碼',
                    ),
                    validator: (password) {
                      if (password != null) {
                        return password.trim().length > 5 ? null : "密码不能少于6位";
                      }
                    }),
              ),
              SizedBox(
                height: 12,
              ),
              OutlinedButton(
                onPressed: () {
                  String email = emailController.text;
                  String password = _pwdController.text;
                  context
                      .read(authServiceProvider)
                      .registerUserByEmail(email, password);
                  if (_formKey.currentState!.validate()) {
                    //验证通过提交数据

                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Processing Data')),
                    );
                  }
                },
                child: Text('完成'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
