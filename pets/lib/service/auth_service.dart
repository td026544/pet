import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// flutter pub run build_runner build
class AuthenticationService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final Reader read;

  AuthenticationService(this.read);
  Stream<User?> get authStateChanges => _firebaseAuth.authStateChanges();
  handleSignIn(User user) {
    if (user != null) {
      // createUserInFireStore();
      print(' isAuth = true;');
      // setState(() {
      //   isAuth = true;
      // });
      print('User is signed in!');
    } else {
      // setState(() {
      //   print(' isAuth = false;');
      //   isAuth = false;
      // });
      print('User is currently signed out!');
    }
    print('handleSignIn()');
  }

  void registerUserByEmail(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      print('RegisterSucees');
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }
  }

  // createUserInFireStore() async {
  //   final userRef = FirebaseFirestore.instance.collection('users');
  //   final user = FirebaseAuth.instance.currentUser;
  //   final DocumentSnapshot doc = await userRef.doc(user?.uid).get();
  //
  //   if (!doc.exists) {
  //     userRef.doc(user?.uid).set({
  //       "id": user?.uid,
  //       "email": user?.email,
  //       "gender": "",
  //       "age": "",
  //       "phoneNumber": user?.phoneNumber,
  //       "nickname": user?.phoneNumber,
  //       "pushToken": "",
  //       "username": user?.displayName,
  //       "createTime": DateTime.now().millisecondsSinceEpoch,
  //       "rooms": [],
  //     });
  //     // Provider.of<UserModel>(context, listen: false)
  //     //     .updateUser(userDoc.get('username'), userDoc.id);
  //
  //     // Navigator.pushNamed(context, WelcomeScreen.id);
  //   } else {}
  // }
}
