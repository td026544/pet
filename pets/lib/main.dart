import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/service/auth_provider.dart';
import 'package:pets/view/home_screen.dart';
import 'package:pets/view/login_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized(); //確認基本初始化完成
  await Firebase.initializeApp(); //FireBase初始化
  runApp(
    ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Root(),
    );
  }
}

//目的要做監聽firebaseUser登入狀態 做導入頁面。
class Root extends StatefulWidget {
  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      AsyncValue fireBaseUser = watch(firebaseUserFutureProvider);
      // final myUser = watch(userStateProvider).state;

      return fireBaseUser.when(
          data: (fireBaseUser) {
            if (fireBaseUser != null) {
              return HomeScreen();
            } else {
              return LoginScreen();
            }
          },
          loading: () => Text('loading'),
          error: (e, s) => Text('error'));
    });
  }
}
