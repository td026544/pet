import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/repositories/pet_repository.dart';

final petListFutureProvider = FutureProvider<List<Pet>?>((ref) async {
  List<Pet> pets = await ref.read(petRepositoryProvider).fetchPets();
  return pets;
});
